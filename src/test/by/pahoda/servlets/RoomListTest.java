package by.pahoda.servlets;

import org.junit.Test;
import org.mockito.Mockito;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RoomListTest extends Mockito {
    private static final String path1 = "/index.jsp";

    @Test
    public void doGet() throws ServletException, IOException {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final RequestDispatcher dispatcher = mock(RequestDispatcher.class);

        when(request.getRequestDispatcher(path1)).thenReturn(dispatcher);

        new RoomList().doGet(request, response);
        verify(dispatcher, times(1)).forward(request, response);

    }

}