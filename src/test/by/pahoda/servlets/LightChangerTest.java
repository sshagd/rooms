package by.pahoda.servlets;

import by.pahoda.entities.Room;
import org.junit.Test;
import org.mockito.Mockito;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LightChangerTest extends Mockito {
    private static final Integer id = 1;

    @Test
    public void doPost() throws IOException {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final HttpSession session = mock(HttpSession.class);
        final Room room = mock(Room.class);

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("currentRoom")).thenReturn(room);
        when(room.getId()).thenReturn(id);

        new LightChanger().doPost(request, response);
        verify(request, times(1)).getSession();
        verify(session, atLeast(1)).getAttribute("currentRoom");
        verify(response, atLeast(1)).sendRedirect("/Rooms/entrance");
    }
}