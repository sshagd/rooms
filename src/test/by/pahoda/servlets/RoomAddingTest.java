package by.pahoda.servlets;

import org.junit.Test;
import org.mockito.Mockito;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RoomAddingTest extends Mockito {
    private static final String name = "name";
    private static final String country = "country";

    @Test
    public void doPost() throws IOException {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);

        when(request.getParameter(name)).thenReturn(name);
        when(request.getParameter(country)).thenReturn(country);

        new RoomAdding().doPost(request, response);
        verify(request, times(1)).getParameter(name);
        verify(request, times(1)).getParameter(country);
        verify(response, atLeast(1)).sendRedirect("/Rooms/");
    }
}