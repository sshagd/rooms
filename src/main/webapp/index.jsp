<%--
  Created by IntelliJ IDEA.
  User: Sasha
  Date: 22.05.2019
  Time: 20:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Список комнат</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
<header class="w3-container w3-blue-grey w3-opacity w3-center" style="height: 70px; align-content: center">
    <label style="display: inline-block; padding: 15px;"><font size="5">Список комнат</font></label>
    <a href="redir" style="display: inline-block; padding: 15px; font-size: large">Создать комнату</a>
</header>

<table style="border: 1px solid black; width: 100%">
    <tr>
        <td>ID комнаты</td>
        <td>Имя комнаты</td>
        <td>Страна</td>
        <td>Посетить</td>
    </tr>
    <%--@elvariable id="rooms" type="java.util.List"--%>
    <c:forEach var="rooms" items="${rooms}">
    <tr>
        <td>${rooms.id}</td>
        <td>${rooms.name}</td>
        <td>${rooms.country}</td>
        <td>
            <form action="entrance" method="post">
                <button type="submit" name="id" value="${rooms.id}" class="w3-btn w3-indigo w3-round-large w3-margin-bottom">Посетить</button>
            </form>
        </td>
        </c:forEach>
    </tr>
</table>

</body>
</html>
