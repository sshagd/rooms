<%--
  Created by IntelliJ IDEA.
  User: Sasha
  Date: 24.05.2019
  Time: 18:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Комната</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<c:choose>
    <c:when test="${currentRoom.light == false}">
        <body style="background-color: darkgray"/>
    </c:when>
    <c:otherwise>
        <body style="background-color: lightyellow"/>
    </c:otherwise>
</c:choose>
<body>
<header class="w3-container w3-blue-grey w3-opacity w3-center" style="height: 70px; align-content: center">
    <a href="index" style="display: inline-block; padding: 15px; font-size: large">Назад</a>
    <label style="display: inline-block; padding: 15px;"><font size="5">Вы вошли в комнату</font></label>
</header>

<%--@elvariable id="currentRoom" type="by.pahoda.entities.Room"--%>
<table style="border: 1px solid black; margin: auto">
    <tr>
        <td style="width: 150px">Имя</td>
        <td style="width: 150px">Страна</td>
        <td style="width: 150px">Лампочка</td>
    </tr>
    <tr>
        <td>${currentRoom.name}</td>
        <td>${currentRoom.country}</td>
        <td>
            <form action="change" method="post">
                <c:choose>
                    <c:when test="${currentRoom.light == false}">
                        <button type="submit" name="id" value="${currentRoom.id}"
                                class="w3-btn w3-indigo w3-round-large w3-margin-bottom">Включить</button>
                    </c:when>
                    <c:otherwise>
                        <button type="submit" name="id" value="${currentRoom.id}"
                                class="w3-btn w3-indigo w3-round-large w3-margin-bottom">Выключить</button>
                    </c:otherwise>
                </c:choose>
            </form>
        </td>
    </tr>
</table>

</body>
</html>
