<%--
  Created by IntelliJ IDEA.
  User: Sasha
  Date: 22.05.2019
  Time: 20:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Создание комнаты</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
<header class="w3-container w3-blue-grey w3-opacity w3-center" style="height: 70px; align-content: center">
    <a href="index" style="display: inline-block; padding: 15px; font-size: large">Назад</a>
    <label style="display: inline-block; padding: 15px;"><font size="5">Создание комнаты</font></label>
    <%--<a href="redir" style="display: inline-block; padding: 15px; font-size: large">Создать комнату</a>--%>
</header>

<form action="room-add" method="post" class="w3-selection w3-light-grey w3-padding">
    <label>Имя комнаты
        <input type="text" name="name" class="w3-input w3-border w3-round-large" style="width: 30%" required><br />
    </label>
    <label>Страна
        <%--<input type="text" name="country" class="w3-input w3-border w3-round-large" style="width: 30%" required><br />--%>
        <br/>
    <select name="country" style="height: 40px; width: 30%">
        <option>Belarus</option>
        <option>Belgium</option>
        <option>China</option>
        <option>Norway</option>
        <option>Russia</option>
        <option>Ukraine</option>
    </select>
    </label>
    <br/>
    <br/>
    <button type="submit" class="w3-btn w3-indigo w3-round-large w3-margin-bottom">Сохранить</button>
</form>

</body>
</html>
