<%--
  Created by IntelliJ IDEA.
  User: Sasha
  Date: 29.05.2019
  Time: 17:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
    <title>Ошибка</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
<header class="w3-container w3-blue-grey w3-opacity w3-center" style="height: 70px; align-content: center">
    <a href="index" style="display: inline-block; padding: 15px; font-size: large">Назад</a>
    <label style="display: inline-block; padding: 15px;"><font size="5">Ошибка</font></label>
    <%--<a href="redir" style="display: inline-block; padding: 15px; font-size: large">Создать комнату</a>--%>
</header>

<h2 style="text-align: center;"><font size="5">Эта комната находится в другой стране.</font></h2>

<form action="index" method="post">
    <button type="submit" class="w3-btn w3-indigo w3-round-large w3-margin-bottom">Вернуться к списку</button>
</form>
</body>
</html>
