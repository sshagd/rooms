package by.pahoda.utils;

import by.pahoda.entities.Room;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class Database {

    public Database() {
    }

    static Statement statement;

    static {
        initRooms();
    }

    static void initRooms(){
        String userName = "root";
        String pass = "password";
        String connectionUrl = "jdbc:mysql://localhost:3306/rooms";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.getMessage();
        }
        try{
            Connection connection = DriverManager.getConnection(connectionUrl, userName, pass);
            statement = connection.createStatement();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.getMessage();
        }
    }

    private List<Room> roomList = new LinkedList<>();

    public List<Room> roomsTable(){
        try {
            ResultSet resultSet = statement.executeQuery("select ROOM_ID, NAME, COUNTRY, LIGHT from room");
            while (resultSet.next()) {
                roomList.add(new Room(resultSet.getInt("ROOM_ID"),
                        resultSet.getString("NAME"),
                        resultSet.getString("COUNTRY"),
                        resultSet.getBoolean("LIGHT")));
            }

        } catch (SQLException e) {
            e.getMessage();
        }
        return roomList;
    }

    public void roomAdd(String name, String country){
        try {
            statement.executeUpdate("insert into room (NAME, COUNTRY) values ('" + name + "', '" + country + "')");
        } catch (SQLException e) {
            e.getMessage();
        }
    }

    private Room current = new Room();

    public Room currentRoom(int id){
        try {
            ResultSet resultSet = statement.executeQuery("select ROOM_ID, name, country, light from room where ROOM_ID = '" + id + "'");
            while (resultSet.next()) {
                current = new Room(resultSet.getInt("ROOM_ID"),
                        resultSet.getString("NAME"),
                        resultSet.getString("COUNTRY"),
                        resultSet.getBoolean("LIGHT"));
            }

        } catch (SQLException e) {
            e.getMessage();
        }
        return current;

    }

    private Boolean currentLight;

    public Boolean light(Integer id){
        try {
            ResultSet resultSet = statement.executeQuery("select light from room where ROOM_ID = '" + id + "'");
            while (resultSet.next()) {
                currentLight = resultSet.getBoolean("LIGHT");
            }
        } catch (SQLException e) {
            e.getMessage();
        }
        return currentLight;
    }

    public void updateLight(Integer id, Integer light){
        try {
            statement.executeUpdate("update room set LIGHT = '" + light + "' where ROOM_ID = '" + id + "'");
        } catch (SQLException e) {
            e.getMessage();
        }
    }

    private String roomCountry;

    public String country(Integer id){
        try {
            ResultSet resultSet = statement.executeQuery("select COUNTRY from room where ROOM_ID = '" + id + "'");
            while (resultSet.next()) {
                roomCountry = resultSet.getString("COUNTRY");
            }
        } catch (SQLException e) {
            e.getMessage();
        }
        return roomCountry;
    }

}
