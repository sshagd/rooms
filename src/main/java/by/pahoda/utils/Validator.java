package by.pahoda.utils;

import by.pahoda.entities.Room;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Country;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.regex.Pattern;

public class Validator extends HttpServlet {

    private static final String DATABASE_CITY_PATH = "D:/Материалы/Java/GeoLite2-City.mmdb";
    private static final String REGEXP = "[[a-zA-Zа-яА-Я]*/\\s]";
    private static DatabaseReader reader;
    private static InetAddress ipAddress;
    private static CityResponse response;

    private StringBuilder userIP() {
        InetAddress IP = null;
        try {
            IP = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.getMessage();
        }
        String text = String.valueOf(IP);
        Pattern pattern = Pattern.compile(REGEXP);
        String[] res = pattern.split(text);
        StringBuilder result = new StringBuilder();
        for (String s : res) {
            result.append(s);
        }
        return result;
    }

    private String countryInit() {
        File dbFile = new File(DATABASE_CITY_PATH);
        {
            try {
                reader = new DatabaseReader.Builder(dbFile).build();
            } catch (IOException e) {
                e.getMessage();
            }
        }
        {
            try {
                ipAddress = InetAddress.getByName(String.valueOf(userIP()));
            } catch (UnknownHostException e) {
                e.getMessage();
            }
        }
        {
            try {
                response = reader.city(ipAddress);
            } catch (IOException | GeoIp2Exception e) {
                e.printStackTrace();
            }
        }
        Country country;
        if(response == null){
            return null;
        }else {
            country = response.getCountry();
        }
        return country.getName();
    }

    public Boolean validation(HttpServletRequest request){
        boolean result = false;
        HttpSession session = request.getSession();
        Room room = (Room) session.getAttribute("currentRoom");
        Integer id = room.getId();
        String country = countryInit();
        if(country != null){
            Database temp = new Database();
            String roomCountry = temp.country(id);
            if (roomCountry.equals(country)){
                result = true;
            }
        } else {
            result = true;
        }
        return result;
    }

}
