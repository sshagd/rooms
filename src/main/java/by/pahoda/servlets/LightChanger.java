package by.pahoda.servlets;

import by.pahoda.entities.Room;
import by.pahoda.utils.Database;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/change")
public class LightChanger extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        Room room = (Room) session.getAttribute("currentRoom");
        Integer id = room.getId();
        Database temp = new Database();
        Boolean light = temp.light(id);
        Integer newLight = (light) ? 0 : 1;
        temp.updateLight(id, newLight);
        resp.sendRedirect("/Rooms/entrance");
    }
}
