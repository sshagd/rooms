package by.pahoda.servlets;

import by.pahoda.utils.Database;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/room-add")
public class RoomAdding extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.setCharacterEncoding("UTF-8");
        String name = req.getParameter("name");
        String country = req.getParameter("country");
        Database temp = new Database();
        temp.roomAdd(name, country);
        resp.sendRedirect("/Rooms/");
    }
}
