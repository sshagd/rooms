package by.pahoda.servlets;

import by.pahoda.entities.Room;
import by.pahoda.utils.Database;
import by.pahoda.utils.Validator;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/entrance")
public class Entrance extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Room room = (Room) session.getAttribute("currentRoom");
        Integer id = room.getId();
        entrance(req, resp, id, session);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Integer id = Integer.valueOf(req.getParameter("id"));
        entrance(req, resp, id, session);
    }

    private void entrance(HttpServletRequest req, HttpServletResponse resp, Integer id, HttpSession session) throws ServletException, IOException {
        Database temp = new Database();
        Room currentRoom = temp.currentRoom(id);
        session.setAttribute("currentRoom", currentRoom);
        if(new Validator().validation(req)){
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/currentRoom.jsp");
        requestDispatcher.forward(req, resp);
        } else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/error.jsp");
            requestDispatcher.forward(req, resp);
        }
    }

}
