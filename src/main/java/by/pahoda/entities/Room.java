package by.pahoda.entities;

public class Room {
    private Integer id;
    private String name;
    private String country;
    private boolean light;

    public Room() {
    }

    public Room(Integer id, String name, String country, boolean light) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.light = light;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public boolean isLight() {
        return light;
    }

}
