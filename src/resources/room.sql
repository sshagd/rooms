create database rooms;
use rooms;
create table room (ROOM_ID INT NOT NULL auto_increment primary key, NAME varchar(50) not null,
COUNTRY varchar(20) not null, LIGHT boolean not null default false);
insert into room (NAME, COUNTRY, LIGHT) values ('ROOM322', 'Belarus', true);
select * from room;